import Home from './components/Home';
import Login from './pages/Login.vue';
import Services from './pages/Services.vue';
import Register from './pages/Register.vue';
import Cabinet from './pages/Cabinet.vue';
import Shop from './pages/Shop.vue';
import Cart from './pages/Cart.vue';

export const routes = [{
        path: '/',
        component: Home,
    }, {
        path: '/login',
        component: Login,
    }, {
        path: '/services',
        component: Services
    },
    {
        path: '/register',
        component: Register
    }, {

        path: '/cabinet',
        component: Cabinet

    },{
        path: '/shop',
        component: Shop
    },{
        path:'/cart',
        component:Cart
    }
];
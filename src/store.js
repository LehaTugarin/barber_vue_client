import {
    getLocalUser
} from "./helpers/auth";

const user = getLocalUser();



export default {
    state: {
        currentUser: user,
        isLoggedIn: !!user,
        loading: false,
        auth_error: null,
        services: [],
        basket: []
    },
    getters: {
        isMobile(state) {
            if (navigator.userAgent.match(/Android/i) ||
            navigator.userAgent.match(/webOS/i) ||
            navigator.userAgent.match(/iPhone/i) ||
            navigator.userAgent.match(/iPad/i) ||
            navigator.userAgent.match(/iPod/i) ||
            navigator.userAgent.match(/BlackBerry/i) ||
            navigator.userAgent.match(/Windows Phone/i)) {
                return true
            } else {
                return false
            }
        },
        isLoading(state) {
            return state.loading;
        },
        isLoggedIn(state) {
            return state.isLoggedIn;
        },
        currentUser(state) {
            return state.currentUser;
        },
        authError(state) {
            return state.auth_error;
        },
        services(state) {
            return state.services;
        },
        basket(state) {
            return state.basket
        },
        searchInBasket: state => id => {
            return state.basket.find(product=>product.id===id);
        }

    },
    mutations: {
        loading(state){
            state.loading = true;
        },
        loadingDone(state){
            state.loading = false;
        },
        login(state) {
            state.loading = true;
            state.auth_error = null;
        },
        loginSuccess(state, payload) {
            state.auth_error = null;
            state.isLoggedIn = true;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.user, {
                token: payload.access_token
            });

            localStorage.setItem("user", JSON.stringify(state.currentUser));
        },
        loginFailed(state, payload) {
            state.loading = false;
            state.auth_error = payload.error;
        },
        logout(state) {
            localStorage.removeItem("user");
            state.isLoggedIn = false;
            state.currentUser = null;
        },
        updateServices(state, payload) {
            state.services = payload;
        },

        updateUser(state, payload) {
            state.currentUser = Object.assign({}, payload, {
                token: state.currentUser.token
            });

            localStorage.setItem("user", JSON.stringify(state.currentUser));
        },
        updateBasket(state, payload) {
            state.basket = payload;
        },
        addInBasket(state, payload) {
            state.basket.push(payload);
        }
    },
    actions: {
        loading(context){
            context.commit("loading");
        },
        loadingDone(context){
            context.commit("loadingDone");
        },
        login(context) {
            context.commit("login");
        },

        updateUser(context) {
            axios.post('/api/auth/me', {
                    headers: {
                        "Authorization": `Bearer ${context.state.currentUser.token}`
                    }
                })
                .then((response) => {

                    console.log('user data is->', response.data);
                    context.commit('updateUser', response.data);
                }).catch((response) => {
                    console.log('error is->', response);
                })
        },

        getServices(context) {
            axios.get('/api/public/services', {
                    /*headers:{
                        "Authorization": `Bearer ${context.state.currentUser.token}`
                    }*/
                })
                .then((response) => {

                    console.log('response is->', response);

                    if (response.data.status == 'OK')
                        context.commit('updateServices', response.data.services);
                }).catch((response) => {
                    console.log('error is->', response);
                })
        }
    }
};
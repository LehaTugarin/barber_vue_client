import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader

import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import Vuetify from 'vuetify'


import VueRouter from 'vue-router';
import Vuex from 'vuex';
import {routes} from './routes';
import StoreData from './store';
import {initialize} from './helpers/general';


window._ = require('lodash');
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.baseURL = 'http://127.0.0.1:8000';

Vue.use(Vuetify,{
  iconfont: 'mdi', // 'md' || 'mdi' || 'fa' || 'fa4',
});

Vue.config.productionTip = false;
Vue.use(VueRouter);
Vue.use(Vuex);

const store = new Vuex.Store(StoreData);

const router = new VueRouter({
    routes,
    mode:'history'
});



initialize(store, router);

new Vue({
  iconfont: 'mdi',
  router,
  store,
  render: h => h(App),
}).$mount('#app')
